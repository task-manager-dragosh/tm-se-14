package ru.dragosh.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.Repository;
import ru.dragosh.tm.entity.Project;

import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ProjectRepository implements Repository<Project> {

    @NotNull
    private EntityManager entityManager;

    public ProjectRepository(@Nullable EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    public List<Project> findAll(String userId) {
        return entityManager.createQuery("select * from project where user_id = :userId", Project.class).setParameter("userId", userId).getResultList();
    }

    public Project find(String projectName, String userId) {
        return entityManager.createQuery("select * from project where user_id = :userId and name = :projectName", Project.class)
                .setParameter("userId",userId)
                .setParameter("projectName", projectName)
                .getSingleResult();
    }

    public void removeAll(String userId) {
        entityManager.remove(entityManager.find(Project.class, userId));
    }

    public List<Project> findByStringPart(String userId, String str) {
        return entityManager.createQuery("select * from project where user_id = :userId and regexp_like (project_name, :str)", Project.class)
                            .setParameter("userId", userId)
                            .setParameter("str", str)
                            .getResultList();
    }

    @Override
    public void persist(Project entity) {
        entityManager.persist(entity);
    }

    @Override
    public void merge(Project entity) {
        entityManager.merge(entity);
    }

    @Override
    public void remove(String userId, String entityId) {
        entityManager.createQuery("delete from project where user_id = :userId and id = :entityId", Project.class)
                    .setParameter("userId", userId)
                    .setParameter("entityId", entityId)
                    .executeUpdate();
    }

    @Nullable
    @Override
    public List<Project> getEntitiesList() {
        return entityManager.createQuery("select * from project", Project.class)
                            .getResultList();
    }

    @Nullable
    @Override
    public List<Project> getSortedBySystemTime(String userId) {
        return entityManager.createQuery("select * from project where user_id = :userId", Project.class).setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public List<Project> getSortedByDateStart(String userId) {
        return entityManager.createQuery("select * from project where user_id = :userId", Project.class).setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public List<Project> getSortedByDateFinish(String userId) {
        return entityManager.createQuery("select * from project where user_id = :userId", Project.class).setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public List<Project> getSortedByStatus(String userId) {
        return entityManager.createQuery("select * from project where user_id = :userId", Project.class).setParameter("userId", userId).getResultList();
    }
}