package ru.dragosh.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.Repository;
import ru.dragosh.tm.entity.Task;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public class TaskRepository implements Repository<Task> {

    @NotNull
    private EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Select("select * from task user_id = #{userId} and project_id = #{project_id}")
    public List<Task> findAll(String userId, String projectId) {
        return entityManager.createQuery("select * from task where user_id = :userId and project = :projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Select("select * from task user_id = #{userId}")
    public Task find(String userId, String projectId, String nameTask) {
        return entityManager.createQuery("select * from task where user_id = :userId and project = :projectId and task_name = :nameTask", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .setParameter("nameTask", nameTask)
                .getSingleResult();
    }

    public void removeAll(String userId, String projectId) {
        entityManager.createQuery("delete from task where user_id = :userId and project = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Nullable
    public List<Task> findByStringPart(String userId, String projectId, String str) {
        return entityManager.createQuery("select * from task user_id = :userId and project_id = :projectId and regexp_like(task_name, :str)", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .setParameter("str", str)
                .getResultList();
    }

    @Override
    public void persist(Task entity) throws ParseException {
        entityManager.persist(entity);
    }

    @Override
    public void merge(Task entity) {
        entityManager.merge(entity);
    }

    @Override
    public void remove(String userId, String entityId) {
        entityManager.createQuery("delete from task where user_id = :userId and id = :entityId ", Task.class)
                .setParameter("userId", userId)
                .setParameter("entityId", entityId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<Task> getEntitiesList() {
        return entityManager.createQuery("select * from task", Task.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> getSortedBySystemTime(String userId) {
        return entityManager.createQuery("select * from task", Task.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> getSortedByDateStart(String userId) {
        return entityManager.createQuery("select * from task", Task.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> getSortedByDateFinish(String userId) {
        return entityManager.createQuery("select * from task", Task.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> getSortedByStatus(String userId) {
        return entityManager.createQuery("select * from task", Task.class)
                .getResultList();
    }
}