package ru.dragosh.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.entity.User;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class UserRepository {

    @NotNull
    private EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    public User find(String login, String password) {
        return entityManager.createQuery("select * from app_user where login = :login and hash_password = :password", User.class)
                .setParameter("login", login)
                .setParameter("password", password)
                .getSingleResult();
    };

    @Nullable
    public User findByLogin(String login) {
        return entityManager.find(User.class, login);
    }

    @Nullable
    public User findById(String id) {
        return entityManager.find(User.class, id);
    }

    public void persist(User user) {
        entityManager.persist(user);
    }

    public void merge(User user) {
        entityManager.merge(user);
    }

    public void remove(String id) {
        entityManager.remove(entityManager.find(User.class, id));
    }

    @Nullable
    public List<User> getEntitiesList() {
        return entityManager.createQuery("select * from app_user", User.class).getResultList();
    }
}
