package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.exception.AccessForbiddenException;
import ru.dragosh.tm.exception.EntityIsAlreadyExistException;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepository {

    @NotNull
    private EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    @Nullable
    public List<Session> findAll() {
        return entityManager.createQuery("select * from session",Session.class).getResultList();
    }

    @Nullable
    public Session findOne(@NotNull String userId) throws Exception {
        return entityManager.find(Session.class, userId);
    }

    public void persist(@NotNull Session session) throws EntityIsAlreadyExistException, AccessForbiddenException {
        entityManager.persist(session);
    }

    public void merge(@NotNull String id, @NotNull Session session) {
        entityManager.merge(session);
    }

    public void remove(@NotNull String id) {
        entityManager.remove(entityManager.find(Session.class, id));
    }

    public void removeAll() {
        entityManager.createQuery("delete from session", Session.class).executeUpdate();
    }
}
