package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.repository.SessionRepository;
import ru.dragosh.tm.repository.TaskRepository;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.util.HibernateUtil;
import ru.dragosh.tm.util.MyBatisUtil;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;

public final class TaskServiceImplement implements TaskService {
    @NotNull
    private final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
    @NotNull
    private final TaskRepository taskRepository = new TaskRepository(entityManager);

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId,
                              @NotNull final String projectId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        if (projectId == null || projectId.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Task> list = taskRepository.findAll(userId, projectId);
        entityManager.getTransaction().commit();
        return list;
    }

    @Nullable
    @Override
    public Task find(@NotNull final String userId,
                     @NotNull final String projectId,
                     @NotNull final String nameTask) {
        if (userId == null || userId.isEmpty())
            return null;
        if (projectId == null || projectId.isEmpty())
            return null;
        if (nameTask == null || nameTask.isEmpty())
            return null;
        entityManager.getTransaction().begin();
        Task task = taskRepository.find(userId, projectId, nameTask);
        entityManager.getTransaction().commit();
        return task;
    }

    @Override
    public void persist(@NotNull final Task task) throws ParseException {
        if (task == null)
            return;
        entityManager.getTransaction().begin();
        taskRepository.persist(task);
        entityManager.getTransaction().commit();
    }

    @Override
    public void merge(@NotNull final Task task) {
        if (task == null)
            return;
        entityManager.getTransaction().begin();
        taskRepository.merge(task);
        entityManager.getTransaction().commit();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String taskId) {
        taskRepository.remove(userId, taskId);
    }

    @Override
    public void removeAll(@NotNull final String userId,
                          @NotNull final String projectId) {
        if (userId == null || userId.isEmpty())
            return;
        if (projectId == null || projectId.isEmpty())
            return;
        entityManager.getTransaction().begin();
        taskRepository.removeAll(userId, projectId);
        entityManager.getTransaction().commit();
    }

    @Override
    public List<Task> getEntitiesList() {
        entityManager.getTransaction().begin();
        List<Task> list = taskRepository.getEntitiesList();
        entityManager.getTransaction().commit();
        return list;
    }

    @Override
    public void loadEntities(@NotNull final List<Task> entities) throws ParseException {
        entityManager.getTransaction().begin();
        for (Task task: entities) {
            taskRepository.persist(task);
        }
        entityManager.getTransaction().commit();
    }

    @NotNull
    @Override
    public List<Task> findByStringPart(@NotNull final String userId,
                                       @NotNull final String projectId,
                                       @NotNull final String str) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        if (projectId == null || projectId.isEmpty())
            return Collections.emptyList();
        if (str == null || str.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Task> list = taskRepository.findByStringPart(userId, projectId, str);
        entityManager.getTransaction().commit();
        return list;
    }

    @NotNull
    @Override
    public List<Task> getSortedBySystemTime(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Task> list = taskRepository.getSortedBySystemTime(userId);
        entityManager.getTransaction().commit();
        return list;
    }

    @NotNull
    @Override
    public List<Task> getSortedByDateStart(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Task> list = taskRepository.getSortedByDateStart(userId);
        entityManager.getTransaction().commit();
        return list;
    }

    @NotNull
    @Override
    public List<Task> getSortedByDateFinish(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Task> list = taskRepository.getSortedByDateFinish(userId);
        entityManager.getTransaction().commit();
        return list;
    }

    @NotNull
    @Override
    public List<Task> getSortedByStatus(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Task> list = taskRepository.getSortedByStatus(userId);
        entityManager.getTransaction().commit();
        return list;
    }
}
