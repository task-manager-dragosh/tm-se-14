package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.repository.ProjectRepository;
import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.util.HibernateUtil;
import ru.dragosh.tm.util.MyBatisUtil;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectServiceImplement implements ProjectService {
    @NotNull
    private final EntityManager entityManager = HibernateUtil.factory().createEntityManager();

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository(entityManager);

    @Override
    public void persist(@NotNull final Project entity) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        entity.setDateStart(dt.format(dt.parse(entity.getDateStart())));
        entity.setDateFinish(dt.format(dt.parse(entity.getDateFinish())));
        entityManager.getTransaction().begin();
        projectRepository.persist(entity);
        entityManager.getTransaction().commit();
    }

    @Override
    public void merge(@NotNull final Project entity) {
        entityManager.getTransaction().begin();
        projectRepository.merge(entity);
        entityManager.getTransaction().commit();
    }

    @Override
    public void remove(@NotNull final String userId,
                       @NotNull final String entityId) {
        entityManager.getTransaction().begin();
        projectRepository.remove(userId, entityId);
        entityManager.getTransaction().commit();
    }

    @NotNull
    @Override
    public List<Project> getEntitiesList() {
        entityManager.getTransaction().begin();
        List<Project> list = projectRepository.getEntitiesList();
        entityManager.getTransaction().commit();
        return list;
    }

    @Override
    public void loadEntities(@NotNull final List<Project> entities) throws ParseException {
        entityManager.getTransaction().begin();
        for (Project project: entities) {
            projectRepository.persist(project);
        }
        entityManager.getTransaction().commit();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Project> list = projectRepository.findAll(userId);
        entityManager.getTransaction().commit();
        return list;
    }

    @Nullable
    @Override
    public Project find(@NotNull final String projectName,
                        @NotNull final String userId) {
        if (projectName == null || projectName.isEmpty())
            return null;
        if (userId == null || userId.isEmpty())
            return null;
        entityManager.getTransaction().begin();
        Project project = projectRepository.find(projectName, userId);
        entityManager.getTransaction().commit();
        return project;
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return;
        entityManager.getTransaction().begin();
        projectRepository.removeAll(userId);
        entityManager.getTransaction().commit();
    }

    @NotNull
    @Override
    public List<Project> findByStringPart(@NotNull final String userId,
                                          @NotNull final String str) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        if (str == null || str.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Project> list = projectRepository.findByStringPart(userId, str);
        entityManager.getTransaction().commit();
        return list;
    }

    @NotNull
    @Override
    public List<Project> getSortedBySystemTime(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Project> list =  projectRepository.getSortedBySystemTime(userId);
        entityManager.getTransaction().commit();
        return list;
    }

    @NotNull
    @Override
    public List<Project> getSortedByDateStart(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Project> list =  projectRepository.getSortedByDateStart(userId);
        entityManager.getTransaction().commit();
        return list;
    }

    @NotNull
    @Override
    public List<Project> getSortedByDateFinish(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Project> list = projectRepository.getSortedByDateFinish(userId);
        entityManager.getTransaction().commit();
        return list;
    }

    @NotNull
    @Override
    public List<Project> getSortedByStatus(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        entityManager.getTransaction().begin();
        List<Project> list = projectRepository.getSortedByStatus(userId);
        entityManager.getTransaction().commit();
        return list;
    }
}
