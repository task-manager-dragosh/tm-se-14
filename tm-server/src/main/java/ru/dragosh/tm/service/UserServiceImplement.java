package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.repository.TaskRepository;
import ru.dragosh.tm.repository.UserRepository;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.util.HibernateUtil;
import ru.dragosh.tm.util.MyBatisUtil;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserServiceImplement implements UserService {
    @NotNull
    private final EntityManager entityManager = HibernateUtil.factory().createEntityManager();

    @NotNull
    private final UserRepository userRepository = new UserRepository(entityManager);

    @Nullable
    @Override
    public User find(@NotNull final String login,
                     @NotNull final String password) {
        if (login == null || login.isEmpty())
            return null;
        if (password == null || password.isEmpty())
            return null;
        entityManager.getTransaction().begin();
        User user = userRepository.find(login, password);
        entityManager.getTransaction().commit();
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login == null || login.isEmpty())
            return null;
        entityManager.getTransaction().begin();
        User user = userRepository.findByLogin(login);
        entityManager.getTransaction().commit();
        return user;
    }

    @Override
    public User findById(@NotNull final String id) {
        entityManager.getTransaction().begin();
        User user = userRepository.findById(id);
        entityManager.getTransaction().commit();
        return user;
    }

    @Override
    public void persist(@NotNull final User user) {
        if (user == null)
            return;
        entityManager.getTransaction().begin();
        userRepository.persist(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public void merge(@NotNull final User user) {
        if (user == null)
            return;
        entityManager.getTransaction().begin();
        userRepository.merge(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public void remove(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return;
        entityManager.getTransaction().begin();
        userRepository.remove(userId);
        entityManager.getTransaction().commit();
    }

    @NotNull
    @Override
    public List<User> getEntitiesList() {
        entityManager.getTransaction().begin();
        List<User> list = userRepository.getEntitiesList();
        entityManager.getTransaction().commit();
        return list;
    }

    @Override
    public void loadEntities(@NotNull final List<User> entities) {
        if (entities == null)
            return;
        entityManager.getTransaction().begin();
        for (User user: entities) {
            userRepository.persist(user);
        }
        entityManager.getTransaction().commit();
    }
}
