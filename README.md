#### 1. ТРЕБОВАНИЯ К SOFTWARE.

- Windows 10.

- Java SE Runtime Environment 8.

#### 2. СТЕК ТЕХНОЛОГИЙ.

- Java SE 1.8.
- Фреймворк Apache Maven 3.6.0.

#### 3. Разработчик.

- Драгош Владимир.

- s414755@gmail.com

#### 4. КОМАНДЫ ДЛЯ СБОРКИ ПРИЛОЖЕНИЯ.

```
mvn clean install
```

#### 5. КОМАНДЫ ДЛЯ ЗАПУСКА ПРИЛОЖЕНИЯ.

```
java -jar ./<.jar file>
```

