package ru.dragosh.tm;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.bootstrap.Bootstrap;

public final class Application {
    public static void main(String[] args) {
        @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}